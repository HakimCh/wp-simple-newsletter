<?php
/**
 * Created by PhpStorm.
 * User: Hakim
 * Date: 31/07/2017
 * Time: 00:50
 */

/**
 * Check if the table exist
 */
function wpsn_check_table_no_exist() {
	global $wpdb;
	$table_name = $wpdb->prefix.WPSN_TABLE_NAME;
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		if(isset($_GET['action']) && $_GET['action'] == 'create_table') {
			printf(
				'<div class="%1$s"><p>%2$s</p></div>',
				esc_attr( 'notice notice-success is-dismissible' ),
				esc_html( __( 'The table was successfully created !' ) )
			);

			$sql = "CREATE TABLE $table_name (
		          id mediumint(9) NOT NULL AUTO_INCREMENT,
		          email text NOT NULL,
		          is_active int(1) DEFAULT 1,
		          created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
		          UNIQUE KEY id (id)
		     ) ". $wpdb->get_charset_collate() . ";";
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			redirection(2,get_admin_url() . "admin.php?page=".PLUGIN_SLUG);
		}
		else {
			printf(
				'<div class="%1$s"><p><strong>%2$s</strong><br>%3$s<br><a href="%4$s">%5$s</a></p></div>',
				esc_attr( 'notice notice-error' ),
				esc_html( __( 'The Table "'.WPSN_TABLE_NAME.'" not exist', 'wpsn-domain' ) ),
				esc_html( __( 'The table is necessary to save subscriber informations', 'wpsn-text-domain' ) ),
				esc_html( get_admin_url() . 'admin.php?page='.PLUGIN_SLUG.'&action=create_table'),
				esc_html( __( 'Create the table', 'wpsn-text-domain' ) )
			);
		}
	}
}

add_action( 'admin_notices', 'wpsn_check_table_no_exist' );
