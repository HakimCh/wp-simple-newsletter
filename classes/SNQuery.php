<?php

use HakimCh\Wordpress\Plugins\SimpleNewsletter;

/**
 * Class SNQuery
 *
 * @see SimpleNewsletter
 *
 * @method static string open($id)
 * @method static string close()
 * @method static string field($id)
 * @method static string buildInlineElement($name, $attr)
 * @method static string buildBlockElement($name, $content, $attr)
 */
class SNQuery {

    private static $instance;

    public static function __callStatic($method, $arguments)
    {
        if(!self::$instance) {
            $forms = file_get_contents(SN_PLUGIN_PATH . 'datas/forms.json');
            self::$instance = new SimpleNewsletter(
                json_decode($forms, true)
            );
        }

        return call_user_func_array([self::$instance, $method], $arguments);
    }
}