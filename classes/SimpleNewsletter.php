<?php

namespace HakimCh\Wordpress\Plugins;

class SimpleNewsletter {

    private $forms = [];
    private $current = null;

    public function __construct($forms)
    {
        $this->forms = $forms;
    }

    public function open($id) {
        if(!array_key_exists($id, $this->forms))
            return null;

        $this->current = $this->forms[$id];


	    return '<form' . $this->buildAttributes($this->current['attr']) . ' data-nsid>';
    }

    public function close() {
        if(is_null($this->current))
            return null;

        return '<div id="wpsn-error-container"></div></form>';
    }

    public function field($id) {
        if(is_null($this->current) || !isset($this->current['fields'][$id]))
            return null;

        $field = $this->current['fields'][$id];

        if($field['type'] == 'inline')
            return $this->buildInlineElement($field['name'], $field['attr']);
        else
            return $this->buildBlockElement($field['name'], $field['content'], $field['attr']);
    }

    public function buildInlineElement($name, $attr)
    {
        return '<' . $name . $this->buildAttributes($attr) . '>';
    }

    public function buildBlockElement($name, $content, $attr)
    {
        return '<' . $name . $this->buildAttributes($attr) . '>' . wp_unslash($content) . '</' . $name . '>';
    }

    private function buildAttributes($attributes)
    {
        $html = '';
        if(is_array($attributes) && !empty($attributes)) {
            foreach($attributes as $key => $value) {
                if(!is_null($value))
                    $html .= ' ' . $key . '="' . $value . '"';
            }
        }
        return $html;
    }
}
