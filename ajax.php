<?php
/**
 * Created by PhpStorm.
 * User: Hakim
 * Date: 31/07/2017
 * Time: 00:57
 */
add_action( 'wp_ajax_nopriv_add_subscriber', 'add_subscriber_ajax_handler' );
add_action( 'wp_ajax_add_subscriber', 'add_subscriber_ajax_handler' );

function add_subscriber_ajax_handler() {
	$alertType = 'danger';

	if(isset($_POST['email'])) {

		$email	= sanitize_text_field($_POST['email']);

		if(filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			global $wpdb;

			$table_name = $wpdb->prefix . WPSN_TABLE_NAME;

			$nbr = $wpdb->get_col("SELECT count(id) as nbr FROM {$table_name} WHERE email = '{$email}'", 0);

			if($nbr[0] == 0) {
				//wp_mail('ab.chmimo@gmail.com', 'New subscriber', $msg, $headers);

				$wpdb->insert($table_name, [
					'email' => $email
				]);

				//redirection(3, get_permalink('url') . '?id=' . $wpdb->insert_id);

				$message = 'We will notify you with our last updates.';
				$alertType = 'success';
			}
			else {
				$message = 'You are already a member!.';
			}
		}
		else {
			$message = 'Please provide a valid email address.';
		}
	}
	else {
		$message = 'All the fields are required.';
	}

	echo json_encode([
		'message' => $message,
		'alertType' => $alertType
	]);
	die();
}