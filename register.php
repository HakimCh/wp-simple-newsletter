<?php
/**
 * Plugin Name: Simple Newsletter
 * Plugin URI:
 * Description: The plugin adds a simple form to subscribe to website newsletter
 * Version: 1.0.0
 * Author: Hakim Chmimo
 * Author URI:
 * Licence: GPL2
 */

define('PLUGIN_SLUG', 'wpsn');
define('WPSN_TABLE_NAME', 'wpsn');
define('SN_PLUGIN_PATH', dirname(__FILE__) . '/');
define('FORM_ASSETS_URL', plugins_url(basename(SN_PLUGIN_PATH) . '/assets/'));

require_once 'classes/SimpleNewsletter.php';
require_once 'classes/SNQuery.php';

// Register form script
function ajax_form_scripts() {
	dump(1);
	wp_enqueue_script( 'validator-script',FORM_ASSETS_URL . 'js/validator.js', array('jquery'), '1.15.0' );
	wp_enqueue_script( 'form-script',FORM_ASSETS_URL . 'js/form.js', array('validator-script') );
}
add_action( 'wp_footer', 'ajax_form_scripts' );

// Include Ajax callback
require 'check.php';

// Include Ajax callback
require 'ajax.php';

// Include admin page
require 'form-admin.php';