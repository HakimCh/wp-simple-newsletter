(function($) {

    $("form[data-nsid]").validate({
        submitHandler: function (form) {
            var email = $('#email', form);
            $.ajax({
                url : ajax_url,
                type : 'post',
                dataType: 'json',
                data : {
                    action : 'add_subscriber',
                    email: email.val()
                },
                success : function( response ) {
                    if(response.alertType === 'success') {
                        email.removeClass('error').val('');
                        var submitBtn = $('button i', form);
                        submitBtn.attr('class', 'ion-android-done');
                        $('#wpsn-error-container').html(response.message);
                        setInterval(function(){
                            submitBtn.attr('class', 'ion-ios-arrow-thin-right');
                            $('#wpsn-error-container').html('');
                        }, 5000);
                    }
                    else {
                        email.addClass('error');
                        $('#wpsn-error-container').html(response.message);
                    }
                }
            });
        },
        rules: {
            email: { required: true, email: true }
        },
        errorPlacement: function ($error, $element) {
            $('#wpsn-error-container').html($error);
        }
    });

})(jQuery);