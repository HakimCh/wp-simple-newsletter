<?php
/**
 * Created by PhpStorm.
 * User: Hakim
 * Date: 31/07/2017
 * Time: 00:56
 */
?>
<div class="wrap">
	<h1 style="margin-bottom:20px;">Manage subscribers</h1>

	<?php
	$showListing = newsletter_actions();
	?>

	<?php if($showListing): ?>

		<?php $results = get_datas_from_table(); ?>

		<div class="tablenav top">
			<div class="alignleft">
				<span class="displaying-num"><?= $results['count']; ?></span>
			</div>
			<div class="alignright navigation navigation-top">
				<?= $results['paginations']; ?>
			</div>
			<br class="clear">
		</div>

		<table class="wp-list-table widefat fixed striped posts">
			<thead>
                <th class="manage-column">E-mail</th>
                <th class="manage-column">Posted At</th>
			</thead>
			<tbody id="the-list">
			<?php if(count($results['datas']) > 0) {
				echo $results['paginations']['current'];
				$admin_url = get_admin_url() . 'admin.php?page='.PLUGIN_SLUG.'&post=';
				foreach($results['datas'] as $result) {
					$deleteUrl 	= wp_nonce_url( $admin_url . $result->id . '&action=disable', 'disable-data_'.$result->id );
					echo '<tr>
							<td>
								<strong class="row-title">' . $result->email . '</strong>
								<div class="row-actions">
									<span class="trash">
										<a class="submitdelete" href="'.$deleteUrl.'">Disable</a>
									</span>
								</div>
							</td>
							<td>' . $result->created_at . '</td>
						</tr>';
				}
			}
			else {
				echo '<tr class="no-items"><td class="colspanchange" colspan="3">No Datas found.</td></tr>';
			}
			?>
			</tbody>
			<tfoot>
                <th class="manage-column">E-mail</th>
                <th class="manage-column">Posted At</th>
			</tfoot>
		</table>

		<div class="navigation navigation-bottom">
			<?= $results['paginations']; ?>
		</div>

	<?php endif; ?>
</div>
