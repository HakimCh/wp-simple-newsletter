<?php
/**
 * Created by PhpStorm.
 * User: Hakim
 * Date: 31/07/2017
 * Time: 00:56
 */

$jsonFilePath = SN_PLUGIN_PATH . 'datas/forms.json';

if(isset($_POST) && !empty($_POST)) {
    printf(
        '<div class="%1$s"><p>%2$s</p></div>',
        esc_attr( 'notice notice-success is-dismissible' ),
        esc_html( __( "Configuration's file was updated successfully", 'wpsn-text-domain' ) )
    );
    $jsonData = json_encode($_POST);
    file_put_contents($jsonFilePath, $jsonData);
}
else {
	$jsonData = file_get_contents($jsonFilePath);
}

$objectData = json_decode($jsonData);
?>
<div class="wrap">
	<h1 style="margin-bottom:20px;">Settings</h1>

    <form method="POST">
        <?php foreach($objectData as $formId => $object): ?>
        <div id="col-container">
            <div id="col-left">
                <div class="col-wrap form-wrap">
                    <h2>Form: <?= $object->attr->id; ?></h2>
                    <?php foreach($object->attr as $key => $value): ?>
                    <div class="form-field">
                        <label><?= $key; ?></label>
                        <input name="<?= $formId; ?>[attr][<?= $key; ?>]" value="<?= $value; ?>" style="width: 100%" />
                     </div>
                    <?php endforeach; ?>
                    <input type="submit" class="button button-primary button-large" value="Update">
                </div>
            </div>
            <div id="col-right">
                <div class="col-wrap form-wrap">
                    <?php foreach($object->fields as $id => $field): ?>
                        <h3>Field: <?= $id; ?></h3>
                        <?php foreach($field as $key => $value): ?>
                            <?php if(is_object($value)): ?>
                                <h4>Field: <?= $id; ?> (Attributes)</h4>
                                <?php foreach($value as $key => $value): ?>
                                    <div class="form-field">
                                        <label><?= $key; ?></label>
                                        <input style="width: 100%" name="<?= $formId; ?>[fields][<?= $id; ?>][attr][<?= $key; ?>]" value="<?= $value; ?>" />
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <div class="form-field">
                                    <label><?= $key; ?></label>
                                    <input style="width: 100%" name="<?= $formId; ?>[fields][<?= $id; ?>][<?= $key; ?>]" value="<?= deslash($value); ?>" />
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    <input type="submit" class="button button-primary button-large" value="Update">
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </form>
</div>
