<?php
/**
 * Created by PhpStorm.
 * User: Hakim
 * Date: 31/07/2017
 * Time: 01:22
 */

$admin_url = get_admin_url() . 'admin.php?page='.PLUGIN_SLUG;
if($confirmed) {
	echo '<div id="message" class="updated notice notice-success is-dismissible">
		<p>
			<strong>The element was successfully disabled!</strong>
			<a href="'.$admin_url.'" class="button">Retrun to the Home Datas</a>
		</p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text">Dismiss this notice.</span>
		</button>
	</div>';
}
else {
	echo '<div class="confirm">
		<h2>Confirmation</h2>
		<p class="msg">Do you really want to disable the following item?</p>
		<p class="li">
			+ <strong>Full Name</strong> : ' . $fullName . '<br />
			+ <strong>Email address</strong> : ' . $company . '
		</p>';
	$deleteUrl = wp_nonce_url( $admin_url . '&post=' . $post_id . '&action=disable&confirm=yes', 'disable-data_'.$post_id );
	echo '<a href="'.$deleteUrl.'" class="button button-danger">I Disable This Item</a> ';
	echo '<a href="'.$admin_url.'" class="button button-warning">Cancel</a>';
	echo '</div>';
}
