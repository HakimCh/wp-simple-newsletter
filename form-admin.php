<?php
/**
 * Created by PhpStorm.
 * User: Hakim
 * Date: 31/07/2017
 * Time: 00:57
 */

add_action('admin_menu', 'newsletter_admin_page');

function newsletter_admin_page() {
	add_menu_page(
		'Manage subscribers',
		'WP Newsletter',
		'manage_options',
		PLUGIN_SLUG,
		'newsletter_subscribers_page',
		'dashicons-email'
	);
	add_submenu_page(
		PLUGIN_SLUG,
		'Settings',
		'Settings',
		'manage_options',
		PLUGIN_SLUG . '_settings',
		'newsletter_subscribers_settings'
	);
}

function newsletter_subscribers_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	require_once dirname(__FILE__) . '/templates/entries.php';
}

function newsletter_subscribers_settings() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	require_once dirname(__FILE__) . '/templates/settings.php';
}

function newsletter_actions() {
	$showListing = true;
	if(isset($_GET['action'], $_GET['post']) && in_array($_GET['action'], ['disable'])) {
		global $wpdb;
		$table_name = $wpdb->prefix . WPSN_TABLE_NAME;
		$post_id 	= intval($_GET['post']);
		$postData 	= $wpdb->get_results("SELECT * FROM {$table_name} WHERE id = '{$post_id}'", OBJECT);
		$admin_url  = get_admin_url() . 'admin.php?page='.PLUGIN_SLUG;
		$confirmed  = $showListing = false;
		if(isset($postData[0]) && !empty($postData[0])) {
			if($_GET['action'] == 'disable' && isset($_GET['_wpnonce']) && check_admin_referer( 'disable-data_'.$post_id )) {
				$fullName = $postData[0]->name;
				$company = $postData[0]->email;
				if(isset($_GET['confirm'])) {
					$confirmed = $showListing = true;
					$wpdb->update(
						$table_name,
						['is_active' => '0'],
						['id' => $post_id]
					);
					redirection(2, $admin_url);
				}
				require_once dirname(__FILE__) . '/templates/confirm-disable.php';
			}
		}
	}
	return $showListing;
}

function get_datas_from_table() {
	global $wpdb;

	$post_per_page = 25;
	$paged = isset($_GET['paged']) ? $_GET['paged'] : 1;
	$offset = ($paged - 1) * $post_per_page;

	$table_name = $wpdb->prefix . WPSN_TABLE_NAME;

	$sql = "SELECT SQL_CALC_FOUND_ROWS * 
    		FROM {$table_name}
    		WHERE is_active = '1'
        	ORDER BY created_at DESC
        	LIMIT ".$offset.", ".$post_per_page."; ";

	$result['datas'] = $wpdb->get_results( $sql, OBJECT);

	$sql_posts_total = $wpdb->get_var( "SELECT FOUND_ROWS();");
	$max_num_pages	 = ceil($sql_posts_total / $post_per_page);

	$result['paginations'] = paginate_links( array(
		'base' => add_query_arg( 'paged', '%#%' ),
		'format' => '',
		'prev_text' => __('&laquo;'),
		'next_text' => __('&raquo;'),
		'total' => $max_num_pages,
		'current' => $paged
	));

	$result['count'] = $sql_posts_total > 0 ? $sql_posts_total . ' items' : $sql_posts_total . ' item';

	return $result;
}

function redirection($time, $url){
	echo '<script type="text/javascript">
		setTimeout(function(){window.location="' . $url . '"}, ' . ($time * 1000) . ');
	</script>';
}
